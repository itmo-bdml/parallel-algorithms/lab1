import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

data = pd.read_csv("../a1_time.csv").reset_index()

sns.set_theme()
plt.title("Task A!. Time Threads dependency")
sns.lineplot(x=data.iloc[:, 1], y=data.iloc[:, 2])
plt.savefig("./img/a1_threads_time_dependency.png")
plt.show()
