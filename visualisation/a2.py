import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

data = pd.read_csv("../a2_time.csv").reset_index()
data1 = pd.read_csv("../a2_1_time.csv").reset_index()
data2 = pd.read_csv("../a2_2_time.csv").reset_index()
data3 = pd.read_csv("../a2_3_time.csv").reset_index()


for i, d in enumerate((data, data1, data2, data3)):
    sns.set_theme()
    sns.lineplot(x=d.iloc[:, 1], y=d.iloc[:, 3])
    plt.title(f"Task A2. Time Threads dependency data_{i}")
    plt.savefig(f"./img/a2_threads_time_dependency_{i}.png")
    plt.show()


sns.set_theme()
plt.title("Task A2. Time efficiency")
sns.lineplot(x=data.iloc[:, 1], y=data.iloc[0, 3] / data.iloc[:, 3])
plt.ylabel("efficiency")
plt.savefig("./img/a2_threads_efficiency.png")
plt.show()