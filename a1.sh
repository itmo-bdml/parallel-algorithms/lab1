#!/bin/bash


touch a1_time.csv
echo "threads, time" >> a1_time.csv
max_threads=10

IFS=$'\n'

for i in `seq 1 $max_threads`
do 
  bash -c "./a1.o $i" >> a1_time.csv
done
unset IFS

