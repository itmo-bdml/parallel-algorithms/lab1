#include "omp.h"
#include <algorithm>
#include <ctime>
#include <iostream>
#include <vector>
#include <chrono>

int main (int argc, char *argv[]) {
  int n = atoi(argv[1]);
  std::srand(unsigned(std::time(nullptr)));

  std::vector<int> v(1e9);
  std::generate(v.begin(), v.end(), std::rand);

  int v_size = v.size();

  long  long i;
  int m=v[0];
  omp_set_dynamic(0);
  auto start = std::chrono::high_resolution_clock::now();
  #pragma omp parallel shared(v) num_threads(n)
  { 
    #pragma omp for reduction(max: m) schedule(static)
    for(i=0; i<v_size; i++) {
      m =std::max(m, v[i]);
    }
  }
  auto stop = std::chrono::high_resolution_clock::now();
  // std::cout<<m<<std::endl;
  auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
  std::cout<<n<<", "<<duration.count()<<std::endl;
  return 0;
}
