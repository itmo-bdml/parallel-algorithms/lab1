# Lab 1 

## Parallel Algorithms Analysis

Itmo university. 1st semester

Performed by: Gleb Mikloshevich J4132C

Lecturer: Timur Sokhin

---

Performance was measured on an Intel i7-1065G7 CPU with 4 cores and 8 threads. Since the processor cannot handle more than 8 threads at a time, the results for 9 and 10 threads do not reflect the actual processing time when using the corresponding number of CPU threads.


#### Task 0

compile the programm

```bash
$ g++ -o a0.o a0.cpp 
```

run 

```bash
$ ./a0.o "Only 3 words"
```

no visulisation

#### Task 1 

compile the programm

```bash
$ g++ -o a1.o a1.cpp --fopenmp 
```

run to generate a csv results file

```bash
$ ./a1.sh
```


<img src="./visualisation/img/a1_threads_time_dependency.png" height="250">

I suppose the processor got another task during the calculation.

#### Task 2

compile the programm

```bash
$ g++ -o a2.o a2.cpp --fopenmp
```

run to generate a csv results file

```bash
$ ./a2.sh
```


<img src="./visualisation/img/a2_threads_time_dependency_0.png" height="250">
<img src="./visualisation/img/a2_threads_time_dependency_1.png" height="250">
<img src="./visualisation/img/a2_threads_time_dependency_2.png" height="250">
<img src="./visualisation/img/a2_threads_time_dependency_3.png" height="250">

I expected the plots to be different from each other, at least due to the data race, but it seems that the g++ compiler optimized the code for each algorithm.

<img src="./visualisation/img/a2_threads_efficiency.png" height="250">

