#!/bin/bash


touch a2_time.csv
echo "threads, size, time" >> a2_time.csv
max_threads=10

IFS=$'\n'

for i in `seq 1 $max_threads`
do 
  bash -c "./a2.o $i 1000" >> a2_time.csv
done
unset IFS

