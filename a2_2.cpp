#include <iostream>
#include "omp.h"
#include <ctime>
#include <stdexcept>
#include <chrono>


int **genEmptyMatrix(int size);
int **genRandomMatrix(int size);
int **matMul(int **a, int **b, int size);

int main(int argc, char *argv[]) {

  int n = atoi(argv[1]);
  int size = 1000;
  if (argc == 3) {
    size = atoi(argv[2]);
  } else if (argc > 3) {
    throw std::invalid_argument("Too many parameters were giver. Expected one or two");
  }
  omp_set_dynamic(0);
  omp_set_num_threads(n);
  int **a = genRandomMatrix(size);
  int **b = genRandomMatrix(size);
  auto start = std::chrono::high_resolution_clock::now();
  int **c = matMul(a, b, size);
  auto stop = std::chrono::high_resolution_clock::now();
  auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
  // std::cout<<n<<' '<<<<duration.count()<<std::endl;
  std::cout << n << ", " << size << ", " << duration.count() << std::endl;
  return 0;
}

int **genEmptyMatrix(int size) {
  int **c;
  c = (int **)malloc(size * sizeof(int *));
  for (int i = 0; i < size; ++i) {
    c[i] = (int *)malloc(size * sizeof(int));
  }
  return c;
}

int **genRandomMatrix(int size) {
  int **m = genEmptyMatrix(size);
  int i, j;
#pragma omp parallel for
  for (i = 0; i < size; ++i) {
    for (j = 0; j < size; ++j) {
      m[i][j] = std::rand() % 100;
    }
  }
  return m;
}

int **matMul(int **a, int **b, int size) {

  int **c = genEmptyMatrix(size);
#pragma omp parallel
  {
  int i, j, k;
  #pragma omp for
    for (k = 0; k < size; ++k) {
      for (i = 0; i < size; ++i) {
        for (j = 0; j < size; ++j) {
          c[i][j] += a[i][k] * b[k][i];
        }
      }
    }
  }
  // std::cout<<"elem0: "<<c[0][0]<<std::endl;
  return c;

}
